from behave import *
from selenium.webdriver.common.by import By
from tests import helpers

trojmiasto_pl = "http://trojmiasto.pl"
horoskop_url = "https://www.trojmiasto.pl/s/Horoskop/"

@given('trojmiasto.pl is avaiable')
def step_impl(context):
    driver = context.driver
    driver.get(trojmiasto_pl)
    assert helpers.is_element_present(driver, By.XPATH, "//a[@title='Trojmiasto.pl']")


@when('user searches for Horoskop')
def step_impl(context):
    driver = context.driver
    driver.find_element_by_id("search_input").send_keys("Horoskop")
    driver.find_element_by_id("search_submit_button").click()


@then('Horoskop for 2020 is returned')
def step_impl(context):
    driver = context.driver
    assert driver.current_url == horoskop_url
    assert helpers.is_element_present(driver, By.LINK_TEXT, "Horoskop")


@then('Horoskop can be opened')
def step_impl(context):
    driver = context.driver
    driver.find_element_by_link_text("Horoskop").click()
    assert driver.find_element_by_xpath("//h1").text == \
           "Całoroczny horoskop dla mieszkańców Trójmiasta na rok 2020"
    driver.find_element_by_xpath("//a[@href='#byk']").click()
    assert helpers.is_element_present(driver, By.ID, "byk")