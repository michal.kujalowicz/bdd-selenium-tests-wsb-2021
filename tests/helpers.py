import os

from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By

from tests import config  # import global properties
from selenium import webdriver
from webdriver_manager.firefox import GeckoDriverManager
from webdriver_manager.chrome import ChromeDriverManager


def new_driver(driver="firefox", implicitly_wait=1):
    if driver == "firefox":
        profile = webdriver.FirefoxProfile()
        profile.set_preference('browser.download.folderList', 2)
        profile.set_preference('browser.download.manager.showWhenStarting', False)
        profile.set_preference('browser.download.dir', os.path.abspath(config.TMP_DIR))
        profile.set_preference('browser.helperApps.neverAsk.saveToDisk', 'text/csv, text/txt, application/vnd.ms-excel')
        profile.set_preference('general.warnOnAboutConfig', False)
        driver = webdriver.Firefox(executable_path=GeckoDriverManager().install(), firefox_profile=profile)
    if driver == "chrome":
        # driver = webdriver.Chrome()
        options = ChromeOptions()
        # the path must be absolute, otherwise Chrome won't download the file
        options.add_experimental_option("prefs", {"download.default_directory": os.path.abspath(config.TMP_DIR)})
        driver = webdriver.Chrome(executable_path=ChromeDriverManager().install(), chrome_options=options)
    driver.implicitly_wait(implicitly_wait)
    driver.maximize_window()
    return driver


def is_element_present(driver, how, what):
    try:
        driver.find_element(by=how, value=what)
    except NoSuchElementException:
        return False
    return True


def logout_if_logged_in(driver):
    if is_element_present(driver, By.LINK_TEXT, "Logout"):
        driver.find_element_by_xpath("//a[@href='/logout']").click()


def login(driver):
    driver.find_element_by_xpath("//a[@href='/signin']").click()
    driver.find_element_by_id("inputEmail").clear()
    driver.find_element_by_id("inputEmail").send_keys(config.USER_EMAIL)
    driver.find_element_by_id("inputPassword").clear()
    driver.find_element_by_id("inputPassword").send_keys(config.USER_PASSWORD)
    driver.find_element_by_id("signin").click()


def create_user(driver):
    driver.find_element_by_xpath("//a[@href='/signup']").click()
    driver.find_element_by_id("name").send_keys(config.USER_NAME)
    driver.find_element_by_id("email").send_keys(config.USER_EMAIL)
    driver.find_element_by_id("password").send_keys(config.USER_PASSWORD)
    driver.find_element_by_id("confirmedPassword").send_keys(config.USER_PASSWORD)
    driver.find_element_by_id("signup").click()
