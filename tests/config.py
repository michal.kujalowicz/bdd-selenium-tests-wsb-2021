"""
Global configuration for all samples and tasks
"""
import os

# Project settings
PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__)) + os.sep + ".." + os.sep
SCREENSHOTS_DIR = PROJECT_ROOT + "screenshots"
TMP_DIR = PROJECT_ROOT + "tmp"

# Append to PATH (if you want your local drivers to be used in the project)
os.environ["PATH"] += os.pathsep + PROJECT_ROOT + "selenium-drivers"

# Application root url
APP_URL = "http://localhost:9998/"
BASE_URL = APP_URL + "?language=en"
USER_NAME = "test1"
USER_EMAIL = "test1@icm"
USER_PASSWORD = "Test123!"

CLEAN_DB_URL = APP_URL + "/setup?action=do"
